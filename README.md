# NETIOT Platform

Docker helper to start all the necessary services: zookeeper, kafka, postgres, redis, cassandra, elasticsearch.

Configure according to your own machine.

For more details, checkout the projects wiki page.